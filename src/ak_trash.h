/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_src_trash_h
#define ak_src_trash_h

#include "ak_common.h"

void _assetkit_hide
ak_trash_init();

void _assetkit_hide
ak_trash_deinit();

#endif /* ak_src_trash_h */
