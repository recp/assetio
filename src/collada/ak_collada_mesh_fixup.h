/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_collada_mesh_fixup_h
#define ak_collada_mesh_fixup_h

#include "ak_collada_common.h"

AkResult _assetkit_hide
ak_dae_mesh_fixup(AkMesh * mesh);

#endif /* ak_collada_mesh_fixup_h */
