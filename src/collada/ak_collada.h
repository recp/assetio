/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef __libassetkit__ak_collada__h_
#define __libassetkit__ak_collada__h_

#include "../../include/assetkit.h"

#include <stdlib.h>

AkResult _assetkit_hide
ak_dae_doc(AkDoc ** __restrict dest,
           const char * __restrict file);

#endif /* __libassetkit__ak_collada__h_ */
