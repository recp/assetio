/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_collada_accessor_h
#define ak_collada_accessor_h

#include "../ak_collada_common.h"

AkResult _assetkit_hide
ak_dae_accessor(AkXmlState * __restrict xst,
                void * __restrict memParent,
                AkAccessor ** __restrict dest);

#endif /* ak_collada_accessor_h */
