/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef __libassetkit__ak_collada_enums_h_
#define __libassetkit__ak_collada_enums_h_

#include "../../../include/assetkit.h"

AkEnum _assetkit_hide
ak_dae_enumInputSemantic(const char * name);

AkEnum _assetkit_hide
ak_dae_enumMorphMethod(const char * name);

AkEnum _assetkit_hide
ak_dae_enumNodeType(const char * name);

#endif /* __libassetkit__ak_collada_fx_enums_h_ */
