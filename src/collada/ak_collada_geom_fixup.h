/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_collada_geom_fixup_h
#define ak_collada_geom_fixup_h

#include "ak_collada_common.h"

AkResult _assetkit_hide
ak_dae_geom_fixup(AkGeometry * geom);

AkResult _assetkit_hide
ak_dae_geom_fixup_all(AkDoc * doc);

#endif /* ak_collada_geom_fixup */
