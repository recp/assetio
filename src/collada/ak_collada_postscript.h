/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_collada_postscript_h
#define ak_collada_postscript_h

#include "ak_collada_common.h"

void _assetkit_hide
ak_dae_postscript(AkXmlState * __restrict xst);

#endif /* ak_collada_postscript_h */
