/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef __libassetkit__ak_collada_fx_enums_h_
#define __libassetkit__ak_collada_fx_enums_h_

#include "../../../include/assetkit.h"

AkEnum _assetkit_hide
ak_dae_fxEnumGlFunc(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumBlend(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumBlendEq(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumGLFace(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumMaterial(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumFog(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumFogCoordSrc(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumFrontFace(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumLightModelColorCtl(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumLogicOp(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumPolyMode(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumShadeModel(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumStencilOp(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumWrap(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumMinfilter(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumMipfilter(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumMagfilter(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumShaderStage(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumFace(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumDraw(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumOpaque(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumChannel(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumRange(const char * name);

AkEnum _assetkit_hide
ak_dae_fxEnumPrecision(const char * name);

#endif /* __libassetkit__ak_collada_fx_enums_h_ */
