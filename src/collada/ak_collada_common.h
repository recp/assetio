/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef __libassetkit__ak_collada_common__h_
#define __libassetkit__ak_collada_common__h_

#include "../../include/assetkit.h"
#include "../ak_libxml.h"
#include "../ak_common.h"
#include "../ak_memory_common.h"
#include "../ak_utils.h"
#include "../ak_tree.h"
#include "ak_collada_strpool.h"

#include <libxml/xmlreader.h>
#include <string.h>

#endif /* __libassetkit__ak_collada_common__h_ */
