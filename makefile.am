#******************************************************************************
# Copyright (c), Recep Aslantas.                                              *
#                                                                             *
# MIT License (MIT), http://opensource.org/licenses/MIT                       *
# Full license can be found in the LICENSE file                               *
#                                                                             *
#******************************************************************************

ACLOCAL_AMFLAGS = -I m4

AM_CFLAGS = -Wall \
            -std=gnu99 \
            -O3 \
            -Wstrict-aliasing=2 \
            -fstrict-aliasing \
            -Wpedantic \
            -Wmissing-declarations \
            -Wno-overlength-strings

lib_LTLIBRARIES = libassetkit.la
libassetkit_la_LDFLAGS = -no-undefined \
                         -version-info 0:1:0 \
                         -lxml2 \
                         -ljemalloc \
                         -lcurl \
                         -luv

include_HEADERS = include/assetkit.h \
                  include/ak-states.h \
                  include/ak-string.h \
                  include/ak-memory.h \
                  include/ak-common.h \
                  include/ak-coord.h \
                  include/ak-coord-util.h \
                  include/ak-lib.h \
                  include/ak-instance.h \
                  include/ak-geom.h \
                  include/ak-transform.h \
                  include/ak-cam.h \
                  include/ak-source.h \
                  include/ak-path.h \
                  include/ak-url.h \
                  include/ak-sid.h \
                  include/ak-options.h \
                  include/ak-trash.h \
                  include/ak-light.h \
                  include/ak-profile.h \
                  include/ak-material.h \
                  include/ak-rb.h \
                  include/ak-map.h \
                  include/ak-util.h \
                  include/ak-bbox.h \
                  include/ak-image.h

libassetkit_la_SOURCES=\
    src/assetkit.c \
    src/ak_common.c \
    src/ak_libxml.c \
    src/ak_memory.c \
    src/ak_memory_ext.c \
    src/ak_sid.c \
    src/ak_utils.c \
    src/ak_string.c \
    src/ak_tree.c \
    src/ak_array.c \
    src/ak_find.c \
    src/ak_memory_rb.c \
    src/ak_memory_lt.c \
    src/ak_lib.c \
    src/ak_id.c \
    src/ak_trash.c \
    src/ak_map.c \
    src/rb.c \
    src/coord_sys/ak_coord_common.c \
    src/coord_sys/ak_coord_mesh.c \
    src/coord_sys/ak_coord_geom.c \
    src/coord_sys/ak_coord_doc.c \
    src/coord_sys/ak_coord_vector.c \
    src/coord_sys/ak_coord_transform.c \
    src/coord_sys/ak_coord_transforms.c \
    src/coord_sys/ak_coord_camera.c \
    src/resc/ak_path.c \
    src/resc/ak_curl.c \
    src/resc/ak_resource.c \
    src/resc/ak_url.c \
    src/instance/ak_inst.c \
    src/default/ak_def_coord.c \
    src/default/ak_def_id.c \
    src/default/ak_def_opt.c \
    src/default/ak_def_cmp.c \
    src/geom/ak_geom_mesh.c \
    src/transform/ak_trans.c \
    src/transform/ak_trans_dup.c \
    src/transform/ak_trans_traverse.c \
    src/lib/ak_lib_geom.c \
    src/mesh/ak_mesh_util.c \
    src/mesh/ak_mesh_index.c \
    src/mesh/ak_mesh_material.c \
    src/mesh/ak_mesh_triangulate.c \
    src/bbox/ak_bbox.c \
    src/bbox/ak_bbox_geom.c \
    src/bbox/ak_bbox_mesh.c \
    src/bbox/ak_bbox_mesh_prim.c \
    src/bbox/ak_bbox_scene.c \
    src/collada/ak_collada.c \
    src/collada/ak_collada_lib.c \
    src/collada/ak_collada_strpool.c \
    src/collada/ak_collada_common.c \
    src/collada/ak_collada_geom_fixup.c \
    src/collada/ak_collada_mesh_fixup.c \
    src/collada/ak_collada_node_fixup.c \
    src/collada/ak_collada_postscript.c \
    src/collada/core/ak_collada_asset.c \
    src/collada/core/ak_collada_technique.c \
    src/collada/core/ak_collada_camera.c \
    src/collada/core/ak_collada_light.c \
    src/collada/core/ak_collada_param.c \
    src/collada/core/ak_collada_annotate.c \
    src/collada/core/ak_collada_value.c \
    src/collada/core/ak_collada_color.c \
    src/collada/core/ak_collada_geometry.c \
    src/collada/core/ak_collada_mesh.c \
    src/collada/core/ak_collada_source.c \
    src/collada/core/ak_collada_vertices.c \
    src/collada/core/ak_collada_enums.c \
    src/collada/core/ak_collada_lines.c \
    src/collada/core/ak_collada_polygons.c \
    src/collada/core/ak_collada_triangles.c \
    src/collada/core/ak_collada_spline.c \
    src/collada/core/ak_collada_controller.c \
    src/collada/core/ak_collada_skin.c \
    src/collada/core/ak_collada_morph.c \
    src/collada/core/ak_collada_visual_scene.c \
    src/collada/core/ak_collada_node.c \
    src/collada/core/ak_collada_evaluate_scene.c \
    src/collada/core/ak_collada_render.c \
    src/collada/core/ak_collada_scene.c \
    src/collada/core/ak_collada_accessor.c \
    src/collada/brep/ak_collada_brep.c \
    src/collada/brep/ak_collada_brep_curve.c \
    src/collada/brep/ak_collada_brep_nurbs.c \
    src/collada/brep/ak_collada_brep_surface.c \
    src/collada/brep/ak_collada_brep_topology.c \
    src/collada/fx/ak_collada_fx_profile.c \
    src/collada/fx/ak_collada_fx_effect.c \
    src/collada/fx/ak_collada_fx_image.c \
    src/collada/fx/ak_collada_fx_technique.c \
    src/collada/fx/ak_collada_fx_color_or_tex.c \
    src/collada/fx/ak_collada_fx_float_or_param.c \
    src/collada/fx/ak_collada_fx_blinn_phong.c \
    src/collada/fx/ak_collada_fx_constant.c \
    src/collada/fx/ak_collada_fx_lambert.c \
    src/collada/fx/ak_collada_fx_pass.c \
    src/collada/fx/ak_collada_fx_states.c \
    src/collada/fx/ak_collada_fx_states_detail.c \
    src/collada/fx/ak_collada_fx_enums.c \
    src/collada/fx/ak_collada_fx_sampler.c \
    src/collada/fx/ak_collada_fx_program.c \
    src/collada/fx/ak_collada_fx_shader.c \
    src/collada/fx/ak_collada_fx_binary.c \
    src/collada/fx/ak_collada_fx_uniform.c \
    src/collada/fx/ak_collada_fx_evaluate.c \
    src/collada/fx/ak_collada_fx_material.c
