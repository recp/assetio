/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef ak_lib_h
#define ak_lib_h
#ifdef __cplusplus
extern "C" {
#endif

AK_EXPORT
AkGeometry *
ak_libFirstGeom(AkDoc * __restrict doc);

#ifdef __cplusplus
}
#endif
#endif /* ak_lib_h */
